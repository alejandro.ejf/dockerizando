FROM ubuntu:latest
LABEL key="Alejandro Jarquin Flores a324223@uach.mx"
COPY ejecutame.sh /ejecutame.sh
RUN ["chmod", "+x", "/ejecutame.sh"]
ENTRYPOINT [ "/ejecutame.sh" ]
